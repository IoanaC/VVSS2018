package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "D:\\Facultate sem 2\\Verificarea si validarea sistemelor soft\\Laboratoare\\Lab1\\02-ProiectSalariati\\ProiectSalariati\\src\\main\\java\\salariati\\employeeDB\\employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) && findEmployeeByCnp(employee.getCnp()) == null) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
		 		e.printStackTrace();
			}
		}
		return false;
	}


	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		List<Employee> employeeList = getEmployeeList();
		int indexFoundEmployee = -1;

		for (int i = 0; i < employeeList.size(); i++)
			if(employeeList.get(i).getCnp().equalsIgnoreCase(oldEmployee.getCnp()))
				indexFoundEmployee = i;

		employeeList.set(indexFoundEmployee,newEmployee);
		writeListInFile(employeeList);

	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}


	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeList = getEmployeeList();
		Collections.sort(employeeList, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				int salari1 = Integer.parseInt(o1.getSalary());
				int salari2 = Integer.getInteger(o2.getSalary());
				if(salari1 > salari2)
					return 1;
				if(salari1 < salari2)
					return -1;
				return o1.getCnp().compareTo(o2.getCnp());
			}
		});
		return employeeList;
	}

	private void writeListInFile(List<Employee> listEmployee) {
		try{
			FileWriter writer = new FileWriter(employeeDBFile, false);
			for (Employee e : listEmployee) {
				writer.write(e.toString());
				writer.write("\n");
			}
			writer.flush();
			writer.close();
		} catch (Exception ex) {
			System.err.println("Error while writing: " + ex);
		}
	}

	public Employee findEmployeeByCnp(String cnp)
	{
		for (Employee e: getEmployeeList()) {
			if(e.getCnp().equalsIgnoreCase(cnp))
				return e;
		}
		return null;
	}
}

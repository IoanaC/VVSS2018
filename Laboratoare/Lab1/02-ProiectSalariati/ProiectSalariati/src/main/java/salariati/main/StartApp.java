package salariati.main;

import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.controller.EmployeeController;
import java.util.Scanner;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {

	static Scanner in = new Scanner(System.in);

	static EmployeeRepositoryInterface employeesRepository = new EmployeeRepositoryImpl();
	static EmployeeController employeeController = new EmployeeController(employeesRepository);

	public static void showMenu()
	{
		System.out.println("1 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare)");
		System.out.println("2 modificarea functiei didactice (ASISTENT/LECTURER/TEACHER/CONFERENTIAR) a unui angajat");
		System.out.println("3 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP)");
		System.out.println("0 EXIT");
		System.out.println();
	}

	public static void main(String[] args) {

		int command;
		System.out.println("---------START--------");
		do {
			showMenu();

			try {
				command = Integer.parseInt(in.nextLine());
			}
			catch (Exception e)
			{
				System.out.println("Comanda incorecta. Indroduceti alta comanda: ");
				command = Integer.parseInt(in.nextLine());
			}

			if(command == 1)
			{
				handleCommand1();
			}

			if(command == 2)
			{
				handleCommand2();
			}

			if(command == 3)
			{
				handleCommand3();
			}
		}while(command != 0);
		System.out.println("----------END---------");

	}

	private static void handleCommand1()
	{
		System.out.println("Dati prenumele: ");
		String prenume = in.nextLine();

		System.out.println("Dati numele: ");
		String nume = in.nextLine();

		System.out.println("Dati CNP: ");
		String cnp = in.nextLine();

		System.out.println("Dati functia didactica(asistent/lector/conferentiar/profesor): ");
		String functieDid =  in.nextLine();

		System.out.println("Dati salariul: ");
		String salar = in.nextLine();

		String stringEmployee = prenume + ";" + nume + ";" + cnp + ";" + functieDid + ";" + salar;
		Employee employee = new Employee();
		try {
			employee = employee.getEmployeeFromString(stringEmployee, 0);
			employeeController.addEmployee(employee);

		}catch (EmployeeException eex)
		{
			System.out.println("Date incorecte..");
		}
		System.out.println();
	}

	private static void handleCommand2()
	{
		System.out.println("Dati CNP-ul angajatului: ");
		String cnp = in.nextLine();

		System.out.println("Dati noua functie didactica(ASISTENT/LECTURER/TEACHER/CONFERENTIAR): ");
		String functieDid =  in.nextLine();
		System.out.println();
		Employee oldEmployee;
		try {
			oldEmployee = employeeController.findEmployeeByCnp(cnp);
			String stringNewEmployee = oldEmployee.getFistName() + ";" + oldEmployee.getLastName() + ";" + cnp + ";" + functieDid + ";" + oldEmployee.getSalary();
			Employee newEmployee = new Employee();
			newEmployee = newEmployee.getEmployeeFromString(stringNewEmployee, 0);
			employeeController.modifyEmployee(oldEmployee,newEmployee);
		}
		catch (EmployeeException eex)
		{
			System.out.println("Datele incorecte...");
			System.out.println();
		}
	}

	private static void handleCommand3()
	{
		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());
		System.out.println();
	}
}
